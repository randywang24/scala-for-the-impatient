/*
*Scala for impatient
* chapter 4 exercises
 */

// setting up global variables
val sampleFile = "/Users/randywang/IdeaProjects/scala-for-the-impatient/" +
  "src/main/resources/sample.txt"

val hamletFile = "/Users/randywang/IdeaProjects/scala-for-the-impatient/" +
  "src/main/resources/hamlet.txt"
/*ex1
Set up a map of prices for a number of gizmos that you covet. Then produce
a second map with the same keys and the prices at a 10 percent discount.
 */
println ("========================ex1========================")
val pricing = Map("AppleWatch" -> 450, "SevenFriday" -> 1250 ,
  "Skateboard" -> 100, "iPad" -> 580, "MacBookPro" -> 3700)

val discountedPricing = for((k, v) <- pricing) yield (k, v * 0.9)
/*ex2
Write a program that reads words from a file. Use a mutable map to count
how often each word appears. To read the words, simply use a java.util.Scanner:
val in = new java.util.Scanner(new java.io.File("myfile.txt"))
while (in.hasNext()) process in.next()
Or look at Chapter 9 for a Scalaesque way.
At the end, print out all words and their counts.
 */
println ("========================ex2========================")
//old school java way to read file
val in = new java.util.Scanner(new java.io.File(sampleFile))
val wordCount = scala.collection.mutable.Map[String, Int]()
while (in.hasNext()) {
  val words = in.next().split("\\s+")
  for (token <- words){
    if (wordCount.contains(token)) wordCount(token) += 1
    else wordCount(token) = 1
  }
}
println(wordCount)

//using method from ch 9
import java.util

import scala.io.Source
val source = Source.fromFile(sampleFile, "UTF-8")
val tokens = source.mkString.split("\\s+")
def counting2 (n : Array[String], m: scala.collection.mutable.Map[String, Int]) = {
  n.foreach( token => if (m.contains(token)) m(token) += 1 else  m(token)=1)
}
val wordCount2 = scala.collection.mutable.Map[String, Int]()
counting2(tokens, wordCount2)
println(wordCount2)
/*ex3
Repeat the preceding exercise with an immutable map
 */
println ("========================ex3========================")
import scala.io.Source

val source3 = Source.fromFile(hamletFile, "UTF-8")
val tokens3 = source3.mkString.split("\\s+")
var wordCount3 = Map[String, Int]()
for (token <- tokens3){
  if(wordCount3.contains(token)) wordCount3 = wordCount3 + (token -> (wordCount3(token)+1))
  else wordCount3 = wordCount3 + (token -> 1)
}
println(wordCount3)

/*ex4
Repeat the preceding exercise with a sorted map, so that the words are
printed in sorted order.
 */
println ("========================ex4========================")
import scala.io.Source

val source4 = Source.fromFile(sampleFile, "UTF-8")
val tokens4 = source4.mkString.split("\\s+")

def counting4(n: Array[String], m:scala.collection.immutable.SortedMap[String, Int]) = {
  var tempMap = scala.collection.immutable.SortedMap[String, Int]()
  for (token <- n){
    if (tempMap.contains(token)) tempMap = tempMap + (token -> (tempMap(token)+1))
    else tempMap = tempMap + (token -> 1)
  }
  tempMap
}

var wordCount4 = counting4(tokens4, scala.collection.immutable.SortedMap[String, Int]())

println(wordCount4)

/*ex5
Repeat the preceding exercise with a java.util.TreeMap that you adapt to the
Scala API.
 */
println ("========================ex5========================")
import scala.collection.JavaConversions.mapAsScalaMap
import scala.io.Source

val source5 = Source.fromFile(sampleFile, "UTF-8")
val tokens5 = source5.mkString.split("\\s+")

var wordCount5 : scala.collection.mutable.Map[String, Int] = new java.util.TreeMap[String, Int]
tokens5.foreach{
  token => wordCount5(token) = wordCount5.getOrElse(token,0) + 1
}
println(wordCount5)
/*ex6
Define a linked hash map that maps "Monday" to java.util.Calendar.MONDAY, and
similarly for the other weekdays. Demonstrate that the elements are visited
in insertion order.
 */
val weekdayMapping  = scala.collection.mutable.LinkedHashMap[String, Int]()
weekdayMapping += ("Monday" -> java.util.Calendar.MONDAY)
weekdayMapping += ("Sunday" -> java.util.Calendar.SUNDAY)
weekdayMapping += ("Friday" -> java.util.Calendar.FRIDAY)
weekdayMapping("Tuesday") = java.util.Calendar.TUESDAY
weekdayMapping("Saturday") = java.util.Calendar.SATURDAY
weekdayMapping("Wednesday") = java.util.Calendar.WEDNESDAY
weekdayMapping += ("Thursday" -> java.util.Calendar.THURSDAY)
weekdayMapping += ("Sunday" -> java.util.Calendar.SUNDAY)

for((k,v) <- weekdayMapping) {
  printf("%s : %d \n", k, v)
}
/*ex7
Print a table of all Java properties, like this:
java.runtime.name | Java(TM) SE Runtime Environment
sun.boot.library.path | /home/apps/jdk1.6.0_21/jre/lib/i386
java.vm.version | 17.0-b16
java.vm.vendor | Sun Microsystems Inc.
java.vendor.url | http://java.sun.com/
path.separator | :
java.vm.name | Java HotSpot(TM) Server VM
You need to find the length of the longest key before you can print the table.
 */
println ("========================ex7========================")
import scala.collection.JavaConversions.propertiesAsScalaMap
val props : scala.collection.Map[String, String] = System.getProperties
val maxLength  = props.keySet.map(_.length).max

for ((k, v) <- props) {

  printf("%-"+ maxLength  + "s | %s\n", k, v)
}


/*ex8
Write a function minmax(values: Array[Int]) that returns a pair containing the
smallest and largest values in the array.
 */
println ("========================ex8========================")

def minmax(values: Array[Int]) : Map[Int, Int] = {
 Map[Int, Int](values.min -> values.max)
}
println(minmax(Array(1, -5 , 10 , 9, 99, -72, -100)))
/*ex9
Write a function lteqgt(values: Array[Int], v: Int) that returns a triple containing
the counts of values less than v, equal to v, and greater than v.
 */
println ("========================ex9========================")
//not so good way
//def lteqgt(values: Array[Int], v: Int) = {
//  var countLess = 0
//  var countEqual = 0
//  var countGreater = 0
//  values.foreach{
//    i => if ( i < v) countLess +=1 else if (i == v) countEqual+=1 else countGreater +=1
//  }
//  (countLess, countEqual, countGreater)
//}
def lteqgt(values: Array[Int], v: Int) = {

  (values.count(_ < v), values.count(_ == v), values.count(_ > v) )
}
println(lteqgt(Array(1, -5 , 10 , 9, 99, -72, -100), 10))
/*ex10
What happens when you zip together two strings, such as "Hello".zip("World")?
Come up with a plausible use case.
 */
println ("========================ex10========================")
val caseConverter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".zip("abcdefghijklmnopqrstuvwxyz").toMap
var lower = caseConverter('D')
println(lower)