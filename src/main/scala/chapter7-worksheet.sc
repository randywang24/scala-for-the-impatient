/*
*Scala for impatient
* chapter 7 exercises
 */

/*ex1
Write an example program to demonstrate that
package com.horstmann.impatient
is not the same as
package com
package horstmann
package impatient
*/

object Ex01 extends App {
}

package com.horstmann {
object Horst {
  val name: String = "horstmann"
}
}

package com {
package horstmann {
package impatient {
class EmployeeA(val name: String) {
  println(Horst.name) //Horst is accessible
}
}
}
}

package com.horstmann.impatient {
class EmployeeB(val name: String) {
  //println(Horst.name) Horst is not accessible
}
}

//package com.horstman
//package anotherclass
//
//object book {
//  val name = "scala for the impatient"
//}
//
////package com.horstmann.impatient
//
//
//
//package com{
//  package horstmann{
//    package impatient{
//    object readerA extends App{
//      println("adfasdfas")
//    }
//    }
//  }
//}



















