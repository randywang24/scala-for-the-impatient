/*
*Scala for impatient
* chapter 5 exercises
 */

/*ex1
Improve the Counter class in Section 5.1, “Simple Classes and Parameterless
Methods,” on page 49 so that it doesn’t turn negative at Int.MaxValue.
*/

class Counter{
  private var value = 0
  def increment() {
    if (value < Int.MaxValue) value += 1
  }
  def current = value
}

val myCounter = new Counter
for (i <- 1 until 55){
  myCounter.increment()
}

println(myCounter.current)
/*ex2
Write a class BankAccount with methods deposit and withdraw, and a read-only
property balance.
 */

class bankAccount{
  private var balance : Double = 0 ;

  def deposit(amount : Double) ={
    balance += amount
  }

  def withdraw(amount : Double) = {
    balance -= amount
  }

  def current = balance

}
val myBankAccount = new bankAccount
println(myBankAccount.current)
myBankAccount.deposit(160.00)
println(myBankAccount.current)
myBankAccount.withdraw(30.00)
println(myBankAccount.current)
/*ex3
Write a class Time with read-only properties hours and minutes and a method
before(other: Time): Boolean that checks whether this time comes before the
other. A Time object should be constructed as new Time(hrs, min), where hrs is in
military time format (between 0 and 23).
 */

class Time (private var hours : Int, private var minutes : Int){

  def setTime(hrs : Int, min: Int) = {
    hours = hrs
    minutes = min
  }

  def before(other: Time) : Boolean = {
    if (hours < other.hours) true
    else if (hours > other.hours) false
    else if (minutes < other.minutes) true
    else false
  }

}

val timeA = new Time(12, 20)
val timeB = new Time(10, 30)
println(timeA.before(timeB))

/*ex4
Reimplement the Time class from the preceding exercise so that the internal
representation is the number of minutes since midnight (between 0 and
24 × 60 – 1). Do not change the public interface. That is, client code should be
unaffected by your change.
 */
class Time2(private var hours: Int, private var minutes: Int){
  private val timestamp : Int = hours*60 + minutes

  def before(other: Time2) : Boolean = {
    if (timestamp < other.timestamp) true else false
  }

}

val time2A = new Time2(12, 20)
val time2B = new Time2(12, 30)
println(time2A.before(time2B))


/*ex5
Make a class Student with read-write JavaBeans properties name (of type String)
and id (of type Long). What methods are generated? (Use javap to check.) Can
you call the JavaBeans getters and setters in Scala? Should you?
 */

import javax.print.attribute.standard.PrinterMoreInfoManufacturer

import scala.beans.BeanProperty


class StudentBean(){
  @BeanProperty  var name : String = _
  @BeanProperty var id : Long = _
}

// results
//Randys-MBP:Desktop randywang$ /Users/randywang/dev/scala/bin/scalac studentBean.scala
//Randys-MBP:Desktop randywang$ javap -private studentBean
//Warning: Binary file studentBean contains StudentBean
//Compiled from "studentBean.scala"
//public class StudentBean {
//  private java.lang.String name;
//  private long id;
//  public java.lang.String name();
//  public void name_$eq(java.lang.String);
//  public void setName(java.lang.String);
//  public long id();
//  public void id_$eq(long);
//  public void setId(long);
//  public java.lang.String getName();
//  public long getId();
//  public StudentBean();
//}

/*ex6
In the Person class of Section 5.1, “Simple Classes and Parameterless Methods,”
on page 49, provide a primary constructor that turns negative ages to 0.
 */
class PersonNew(private var age :Int) {

  age = if (age > 0) age else 0

  def getAge = age
}
val personA = new PersonNew(10)
println(personA.getAge)
val personB = new PersonNew(-10)
println(personB.getAge)


/*ex7
Write a class Person with a primary constructor that accepts a string containing
a first name, a space, and a last name, such as new Person("Fred Smith"). Supply
read-only properties firstName and lastName. Should the primary constructor
parameter be a var, a val, or a plain parameter? Why?
 */

class Person(private val name: String){
  private val firstName : String = name.split(" ")(0)
  private val lastName : String = name.split(" ")(1)

  def getFirstName = firstName
  def getLastName = lastName
}

val personC = new Person("Tom White")
println(personC.getLastName+", "+personC.getFirstName)

/*ex8
Make a class Car with read-only properties for manufacturer, model name,
and model year, and a read-write property for the license plate. Supply four
constructors. All require the manufacturer and model name. Optionally,
model year and license plate can also be specified in the constructor. If not,
the model year is set to -1 and the license plate to the empty string. Which
constructor are you choosing as the primary constructor? Why?
 */

//class Car ( val manufacturer: String, val model : String) {
//
//  private var year : Int = -1
//  var license : String = ""
//
//  def this(year : Int)  {
//    this()
//    this.year = year
//  }
//  def this(license : String){
//    this()
//    this.license = license
//  }
//
//  def this(year: Int, license: String){
//    this(year)
//    this.license = license
//  }
//
//}


//damm, there is a better way

class Car2(val manufacture : String, val model : String, val year : Int = -1, var license : String = ""){

  override def toString = "Car2(%s, %s, %d, %s).format(manufactuer, model, year, license)"
}

val c1 = new Car2("Honda", "Civic", 2011, "xx123zz")
println(c1)
val c2 = new Car2("Hummer", "H1", 2010)
println(c2)
val c3 = new Car2("Opel", "Astra")
println(c3)



/*ex10
Consider the class
class Employee(val name: String, var salary: Double) {
def this() { this("John Q. Public", 0.0) }
}
Rewrite it to use explicit fields and a default primary constructor. Which form
do you prefer? Why?
 */
class Employee1 {
  private var name: String = "John Q. Public"
  private var salary : Double = 0.0

  def this(name: String, salary: Double){
    this()
    this.name = name
    this.salary = salary
  }

  def getInfo: Unit = {
    println("name: "+ name +", salary: "+ salary)
  }
}

val employee1X= new Employee1
employee1X.getInfo
val employee1Y= new Employee1("Randy Wang", 999999.99)
employee1Y.getInfo

class Employee2(val name: String ="John Q. Public", var salary: Double = 0.0) {
  def getInfo = {
    println("name: "+ name +", salary: "+ salary)
  }
}


val employee2X= new Employee1
employee1X.getInfo
val employee2Y= new Employee1("Randy Wang", 999999.99)
employee1Y.getInfo








