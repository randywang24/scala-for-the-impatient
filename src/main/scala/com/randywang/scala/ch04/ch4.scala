package com.randywang.scala.ch04

/**
 * Scala for impatient
 * chapter 4 exercises
 * Created by randywang on 6/21/15.
 */

object ch4 extends App{

  // setting up global variables
  val sampleFile = "/Users/randywang/IdeaProjects/scala-for-the-impatient/" +
    "src/main/resources/sample.txt"

  val hamletFile = "/Users/randywang/IdeaProjects/scala-for-the-impatient/" +
    "src/main/resources/hamlet.txt"


  /*ex1
Set up a map of prices for a number of gizmos that you covet. Then produce
a second map with the same keys and the prices at a 10 percent discount.
 */
  val pricing = Map("AppleWatch" -> 450, "SevenFriday" -> 1250 ,
    "Skateboard" -> 100, "iPad" -> 580, "MacBookPro" -> 3700)

  val discountedPricing = for((k, v) <- pricing) yield (k, v * 0.9)

  println ("========================ex1========================")

  /*ex2
Write a program that reads words from a file. Use a mutable map to count
how often each word appears. To read the words, simply use a java.util.Scanner:
val in = new java.util.Scanner(new java.io.File("myfile.txt"))
while (in.hasNext()) process in.next()
Or look at Chapter 9 for a Scalaesque way.
At the end, print out all words and their counts.
 */

  //old school java way to read file
  val wordCount = scala.collection.mutable.Map[String, Int]()

  val in = new java.util.Scanner(new java.io.File(sampleFile))
  while(in.hasNext()){
    val words = in.next().split("\\s+")
    for (token <- words){
      wordCount(token) = wordCount.getOrElse(token, 0) + 1
    }
  }
  println ("========================ex2-a=======================")
  println(wordCount)

  //using method from ch 9
  import scala.io.Source

  val wordCount2 = scala.collection.mutable.Map[String, Int]()
  val source = Source.fromFile(sampleFile, "UTF-8")
  val tokens = source.mkString.split("\\s+")

//  tokens2.foreach( token => if (wordCount2.contains(token)) wordCount2(token) += 1 else wordCount2(token) = 1)
  tokens.foreach( token => wordCount2(token) = wordCount2.getOrElse(token, 0) + 1 )

  println ("========================ex2-b=======================")
  println(wordCount2)


  /*ex3
Repeat the preceding exercise with an immutable map
 */

  var wordCount3 = Map[String, Int]()
  for (token <- tokens){
    if(wordCount3.contains(token)) wordCount3 += (token -> (wordCount3(token) + 1))
    else wordCount3 += (token -> 1)
  }

  println ("========================ex3========================")
  println(wordCount3)

  /*ex4
  Repeat the preceding exercise with a sorted map, so that the words are
  printed in sorted order.
   */
  var wordCount4 = scala.collection.immutable.SortedMap[String, Int]()

//  for(token <- tokens){
//    if(wordCount4.contains(token)) wordCount4 += token -> (wordCount4(token) +1)
//    else wordCount4 += token -> 1
//  }

  tokens.foreach{
    token => wordCount4 += token -> (wordCount4.getOrElse(token, 0) + 1)
  }

  println ("========================ex4========================")
  println(wordCount4)


  /*ex5
  Repeat the preceding exercise with a java.util.TreeMap that you adapt to the
  Scala API.
   */
  import scala.collection.JavaConversions.mapAsScalaMap

  val wordCount5 : scala.collection.mutable.Map[String, Int] = new java.util.TreeMap[String, Int]()

  tokens.foreach(
  token => wordCount5(token) = wordCount5.getOrElse(token, 0) + 1
  //another way to update mutable map
//  token => wordCount5 += token -> (wordCount5.getOrElse(token,0)+1)
  )

  println ("========================ex5========================")
  println(wordCount5)

  /*ex6
  Define a linked hash map that maps "Monday" to java.util.Calendar.MONDAY, and
  similarly for the other weekdays. Demonstrate that the elements are visited
  in insertion order.
   */
  val weekdayMapping = scala.collection.mutable.LinkedHashMap[String,Int]()
  weekdayMapping += ("Monday" -> java.util.Calendar.MONDAY)
  weekdayMapping += ("Sunday" -> java.util.Calendar.SUNDAY)
  weekdayMapping += ("Friday" -> java.util.Calendar.FRIDAY)
  weekdayMapping("Tuesday") = java.util.Calendar.TUESDAY
  weekdayMapping("Saturday") = java.util.Calendar.SATURDAY
  weekdayMapping("Wednesday") = java.util.Calendar.WEDNESDAY
  weekdayMapping += ("Thursday" -> java.util.Calendar.THURSDAY)
  weekdayMapping += ("Sunday" -> java.util.Calendar.SUNDAY)
  weekdayMapping("Monday") = java.util.Calendar.MONDAY

  println ("========================ex6========================")
  for((k,v) <- weekdayMapping) {
    printf("%s \t: %d \n", k, v)
  }

  /*ex7
  Print a table of all Java properties, like this:
  java.runtime.name | Java(TM) SE Runtime Environment
  sun.boot.library.path | /home/apps/jdk1.6.0_21/jre/lib/i386
  java.vm.version | 17.0-b16
  java.vm.vendor | Sun Microsystems Inc.
  java.vendor.url | http://java.sun.com/
  path.separator | :
  java.vm.name | Java HotSpot(TM) Server VM
  You need to find the length of the longest key before you can print the table.
   */
  val props = scala.collection.JavaConversions.propertiesAsScalaMap(System.getProperties)
  val maxLen = props.keySet.map(_.length).max

  println ("========================ex7========================")

  for ( (k,v) <- props) {
    printf("%-" + maxLen  + "s | %s\n", k, v)
  }

  /*ex8
Write a function minmax(values: Array[Int]) that returns a pair containing the
smallest and largest values in the array.
 */
  def minmax(values : Array[Int]) = {
    (values.min, values.max)
  }

  println ("========================ex8========================")
  println(minmax(Array(1, -5 , 10 , 9, 99, -72, -100)))


  /*ex9
Write a function lteqgt(values: Array[Int], v: Int) that returns a triple containing
the counts of values less than v, equal to v, and greater than v.
 */
  def lteqgt(values : Array[Int], v: Int) = {
    (values.count(_ < v), values.count(_ == v), values.count(_ > v))

  }

  println ("========================ex9========================")
  println(lteqgt(Array(1, -5 , 10 , 9, 99, -72, -100), 10))

  /*ex10
  What happens when you zip together two strings, such as "Hello".zip("World")?
  Come up with a plausible use case.
   */
  val caseMapping = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".zip("abcdefghijklmnopqrstuvwxyz").toMap

  def getLowercase(ch : Char) : Char = {
    caseMapping(ch)
  }

  println ("========================ex10========================")
  printf("The lower case letter for the input %s is: %s", 'R', getLowercase('R') )


}


