package com.randywang.scala.ch07

import java.util.{HashMap => JavaHashMap}
import scala.collection.mutable.{HashMap => ScalaHashMap}
import scala.collection.JavaConversions.mapAsScalaMap


/**
 * Created by randywang on 6/30/15.
 *
 * Write a program that copies all elements from a Java hash map into a Scala
hash map. Use imports to rename both classes.
 */
object ex06 extends App{



  val x =  new JavaHashMap [Int, String] {put(1, "Yo"); put(2, "Bro")}
  val y : ScalaHashMap[Int, String]  = new ScalaHashMap[Int, String]()

  for ((k,v) <- x) {
    y += (k -> v)
  }

  y.foreach(println)
}
