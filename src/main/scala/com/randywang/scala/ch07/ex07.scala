package com.randywang.scala.ch07

/**
 * Created by randywang on 7/3/15.
 *
 * In the preceding exercise, move all imports into the innermost scope possible.
 */
object ex07 extends App{
  import java.util.{HashMap => JavaHashMap}
  import scala.collection.mutable.{HashMap => ScalaHashMap}

  import scala.collection.JavaConversions.mapAsScalaMap


  val x =  new JavaHashMap [Int, String] {put(1, "Yo"); put(2, "Bro")}
  val y : ScalaHashMap[Int, String]  = new ScalaHashMap[Int, String]()

  for ((k,v) <- x) {
    y += (k -> v)
  }

  y.foreach(println)
}
