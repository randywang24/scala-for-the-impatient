package com.randywang.scala.ch07

/**
 * Created by randywang on 6/28/15.
 *
 * Write an example program to demonstrate that
package com.horstmann.impatient
is not the same as
package com
package horstmann
package impatient
 */

object ex01 extends App{

  println("Finished.")
}

package com.horstmann {
object book {
  val name: String = "scala for the impatient"
}
}

package com {
package horstmann {
package impatient {
class EmployeeA(val name: String) {
  println("========")
  println(book.name) //Horst is accessible
}
}
}
}

package com.horstmann.impatient {
class EmployeeB(val name: String) {
//  println(book.name) // Horst is not accessible
}
}