package com.randywang.scala.ch07

/**
 * Created by randywang on 7/3/15.
 * Apart from StringBuilder, what other members of java.lang does the scala
 * package override?
 */
object ex10 {

  /*
scala.BigDecimal
scala.BigInt
scala.Boolean
scala.Iterable
scala.Iterator
scala.Long
scala.Stream
scala.StringBuilder
scala.Vector

The primitive wrappers are overriden, Boolean, Byte, Double, Float, Long and Short
   */
}
