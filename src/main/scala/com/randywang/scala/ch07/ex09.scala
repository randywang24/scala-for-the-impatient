package com.randywang.scala.ch07

/**
 * Created by randywang on 7/3/15.
 *
 * Write a program that imports the java.lang.System class, reads the user name
 *from the user.name system property, reads a password from the Console object,
 *and prints a message to the standard error stream if the password is not
 *"secret". Otherwise, print a greeting to the standard output stream. Do not use
 *any other imports, and do not use any qualified names (with dots).
 */

import java.lang.System._
import java.io.IOException

object ex09 extends App{

  try {
    val username = getProperty("user.name")

    val password = Console.readLine("password:")
    //new method
//    val password = io.StdIn.readLine("password:")
    if (password != "secret") {
      err.println("Invalid password!")
    }else{
      println("Welcome %s!".format(username))
    }



  }
  catch {
    case  ioe: IOException => println("io error!!!\n"+ioe)
    case npe: NullPointerException => println("Null Pointer Exception!!!\n"+npe)
    case  e: Exception => println("error!!!\n"+e)
  }


}
