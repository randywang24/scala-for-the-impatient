package com.randywang.scala.ch07

/**
 * Created by randywang on 6/28/15.
 *
 * Write a package random with functions nextInt(): Int, nextDouble(): Double, and
setSeed(seed: Int): Unit. To generate random numbers, use the linear
congruential generator
next = (previous × a + b) mod 2n,
where a = 1664525, b = 1013904223, n = 32, and the inital value of previous
is seed.
 */

package object random {
  val a = 1664525
  val b = 1013904223
  val n = 32

  var curr = 0
  var currD = 0.0

  import math.pow

  def nextInt(): Int = {
    curr = nextDouble().toInt
    curr
  }

  def nextDouble() : Double = {
    currD = (currD * a + b) % pow(2, n)
    currD
  }

  def setSeed(seed: Int): Unit = {
    curr = seed
  }

}

object ex03 extends App {


  println(random.nextDouble())
  println(random.nextDouble())
  println(random.nextDouble())
  println(random.nextDouble())
  println(random.nextDouble())
  println(random.nextInt())
  println(random.nextInt())
  println(random.nextInt())
  println(random.nextInt())
  println(random.nextInt())
}

