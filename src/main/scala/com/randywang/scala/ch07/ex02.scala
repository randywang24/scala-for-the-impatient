package com.randywang.scala.ch07{
/**
 * Created by randywang on 6/28/15.
 *
 * Write a puzzler that baffles your Scala friends, using a package com that isn’t
at the top level.
 */
  package testpackage{
    package com{
      package blah{
        object meh {
          val b = "some string"
        }
      }
    }
  }

  object ex02 extends App {

    val a = 1
    import testpackage.com._
    println (blah.meh.b)
    assert(_root_.com.randywang.scala.ch07.ex02.a == ex02.a)

  }


}



