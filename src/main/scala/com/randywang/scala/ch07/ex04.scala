package com.randywang.scala.ch07

/**
 * Created by randywang on 6/29/15.
 *
 * Why do you think the Scala language designers provided the package object
 * syntax instead of simply letting you add functions and variables to a package?
 */
object ex04 {
  //It's not possible, JVM limitations
}
