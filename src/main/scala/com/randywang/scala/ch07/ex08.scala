package com.randywang.scala.ch07

import java._
/**
 * Created by randywang on 7/3/15.
 *
 * What is the effect of
 * import java._
 * import javax._
 * Is this a good idea?
 */
object ex08 extends App{

  val x = util.Calendar.MONDAY
  val y : lang.Boolean = true

  val a = java.util.Calendar.MONDAY
  val b : java.lang.Boolean = true


  println(x)
  println(y)

  println(a)
  println(b)
}



