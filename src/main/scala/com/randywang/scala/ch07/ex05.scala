package com.randywang.scala.ch07

/**
 * Created by randywang on 6/30/15.
 *
 * What is the meaning of private[com] def giveRaise(rate: Double)? Is it useful?
 */
object ex05 {
  //function accessible only by members of com package. it's not very useful if everything. is in com package.
}
