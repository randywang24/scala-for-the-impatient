/*
*Scala for impatient
* chapter 6 exercises
 */

/*ex1
Write an object Conversions with methods inchesToCentimeters, gallonsToLiters, and
milesToKilometers.
*/


object Conversions {
  def inchesToCentimeters (value : Double) = value * 2.54
  def gallonsToLiters (value : Double) = value * 3.78541178
  def milesToKilometers (value : Double) = value * 1.609344
}

println("inchesToCentimeters:"+Conversions.inchesToCentimeters(1.0))
println("gallonsToLiters:"+Conversions.gallonsToLiters(1.0))
println("milesToKilometers:"+Conversions.milesToKilometers(1.0))
/*ex2
The preceding problem wasn’t very object-oriented. Provide a general superclass
UnitConversion and define objects InchesToCentimeters, GallonsToLiters, and
MilesToKilometers that extend it.
 */

abstract class UnitConversion{
  def convert(value: Double): Double
}
object InchesToCentimeters extends UnitConversion {
  override def convert(value: Double): Double = value * 2.54
}
object GallonsToLiters extends UnitConversion {
  override def convert(value: Double): Double = value * 3.78541178
}
object MilesToKilometers extends UnitConversion {
  override def convert(value: Double): Double = value * 1.609344
}
println("inchesToCentimeters:"+InchesToCentimeters.convert(1.0))
println("gallonsToLiters:"+GallonsToLiters.convert(1.0))
println("milesToKilometers:"+MilesToKilometers.convert(1.0))
//found a way better solution
class UnitConversion2(factor: Double){
  def convert(value: Double) : Double = value * factor
}
object InchesToCentimeters2 extends UnitConversion2(2.54)
object GallonsToLiters2 extends UnitConversion2(3.78541178)
object MilesToKilometers2 extends UnitConversion2(1.609344)
println("inchesToCentimeters:"+InchesToCentimeters2.convert(1.0))
println("gallonsToLiters:"+GallonsToLiters2.convert(1.0))
println("milesToKilometers:"+MilesToKilometers2.convert(1.0))
/*ex3
Define an Origin object that extends java.awt.Point. Why is this not actually a
good idea? (Have a close look at the methods of the Point class.)
 */
object Origin extends java.awt.Point{
}
println(Origin)
/*ex4
Define a Point class with a companion object so that you can construct Point
instances as Point(3, 4), without using new.
 */
class Point(val x: Int, val y: Int){
  override def toString = {
    "The point is: ("+x+", "+y+")"
  }
}
object Point{
  def apply ( x: Int, y: Int) = {
    new Point(x, y)
  }
}
val pointX = Point(13, 4)
pointX.toString

/*ex5
Write a Scala application, using the App trait, that prints the command-line
arguments in reverse order, separated by spaces. For example, scala Reverse
Hello World should print World Hello.
 */

object ReverseArgs extends App {
  val reversedList = args.reverse
  println(reversedList.mkString(" "))

}

/*ex6
Write an enumeration describing the four playing card suits so that the toString
method returns ♣, ♦, ♥, or ♠.
 */
object cards extends Enumeration {
  type cards = Value
  val club  = Value("♣")
  val diamond = Value("♦")
  val heart = Value("♥")
  val spade = Value("♠")

}

println(cards.values)
/*ex7
Implement a function that checks whether a card suit value from the preceding
exercise is red.
 */

def isRed(card : cards.cards) = {
  card == cards.heart || card  == cards.diamond
}
/*ex8
Write an enumeration describing the eight corners of the RGB color cube. As
IDs, use the color values (for example, 0xff0000 for Red).
 */

object RGBCorners extends Enumeration {
  type RGBCorners = Value
  val black = Value(0x000000, "Black")
  val red = Value(0xff0000, "Red")
  val green = Value(0x00ff00, "Green")
  val blue = Value(0x0000ff, "Blue")
  val yellow = Value(0xffff00, "Yellow")
  val magenta = Value(0xff00ff, "Magenta")
  val cyan = Value(0x00ffff, "Cyan")
  val white = Value(0xffffff, "White")
}
for(i <- RGBCorners.values) println(i.id +":"+ i)

