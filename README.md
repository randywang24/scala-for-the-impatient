# README #

Before you start, take a few seconds and read this:

### What is this repository for? ###

* This repo is my solutions to exercises in Cay Horstmann's book "Scala for the Impatient".

### What book? How can I find it? ###
* [Scala for the Impatient](http://logic.cse.unt.edu/tarau/teaching/scala_docs/scala-for-the-impatient.pdf)

### Why the code is not pretty? ###

* Still a noob, still learning.

### Reference  ###
* [https://github.com/hempalex/scala-impatient](https://github.com/hempalex/scala-impatient)
* [https://github.com/BasileDuPlessis/scala-for-the-impatient](https://github.com/BasileDuPlessis/scala-for-the-impatient)

*  Author: RW